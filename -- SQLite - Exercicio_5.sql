SELECT blogpacktor_category.*, COUNT(blogpacktor_post_category.category_id) as blogpacktor_contador
FROM blogpacktor_post_category
JOIN blogpacktor_category ON blogpacktor_category.id = blogpacktor_post_category.category_id
GROUP BY blogpacktor_post_category.category_id
HAVING blogpacktor_contador >= 2