SELECT blogpacktor_category.*, blogpacktor_post.* 
FROM blogpacktor_post_category 
JOIN blogpacktor_category ON blogpacktor_post_category.category_id = blogpacktor_category.id
JOIN blogpacktor_post ON blogpacktor_post_category.post_id = blogpacktor_post.id 
WHERE blogpacktor_post_category.category_id = 3
