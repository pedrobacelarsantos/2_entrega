from django import forms
from .models import *


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'author', 'category', 'body', 'link_da_imagem')

        widgets ={
            'title': forms.TextInput(attrs = {'class': 'form-control'}),
            # 'title_tag': forms.TextInput(attrs = {'class': 'form-control'}),
            'link_da_imagem': forms.TextInput(attrs = {'class': 'form-control'}),
            'author': forms.TextInput(attrs = {'class': 'form-control', 'value': '', 'id': 'user_logado', 'type': 'hidden'}),
            # 'author': forms.Select(attrs = {'class': 'form-control'}),
            # 'category': forms.Select(choices = choice_list, attrs = {'class': 'form-control'}),
            'body': forms.Textarea(attrs = {'class': 'form-control'}),

        }

class EditForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title','category' , 'body', 'link_da_imagem')

        widgets ={
            'title': forms.TextInput(attrs = {'class': 'form-control'}),
            'link_da_imagem': forms.TextInput(attrs = {'class': 'form-control'}),
            # 'author': forms.Select(attrs = {'class': 'form-control'}),
            'body': forms.Textarea(attrs = {'class': 'form-control'}),
            'category': forms.SelectMultiple(attrs = {'class': 'form-control'}),

        }

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('name', 'body')

        widgets ={
            'name': forms.TextInput(attrs = {'class': 'form-control', 'value': '', 'id': 'user_logado_comment', 'type': 'hidden'}),
            'body': forms.Textarea(attrs = {'class': 'form-control'}),

        }



