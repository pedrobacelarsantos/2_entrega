from django.urls import path
from .views import *
from . import views

urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('adicionar_post/', AddPostView.as_view(), name='adicionar_post'),
    path('post/<int:pk>/deletar', DeletePostView.as_view(), name='deletar_post'),
    path('post_conteudo/<int:pk>', ConteudoPostView.as_view(), name="post_conteudo"),
    path('post/<int:pk>/editar', EditarPostView.as_view(), name="editar_post"),
    path('post/<int:pk>/comentario/', AdicionarCommentView.as_view(), name="adicionar_comentario"),
    path('category/<int:category_id>/', views.Category_Posts, name='category'),
    path('categories/', CategoryListView.as_view(), name="categories_separadas"),
    path('categoria_adicionar/', AdicionarCategoryView.as_view(), name='categoria_adicionar'),
]
