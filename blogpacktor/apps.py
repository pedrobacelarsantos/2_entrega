from django.apps import AppConfig


class BlogpacktorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'blogpacktor'
