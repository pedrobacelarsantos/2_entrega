from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from blogpacktor.models import Post
from .models import *
from .forms import *
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404

# Create your views here.
class HomeView(ListView):
    model = Post
    template_name = 'home.html'
    # ordering = ['-id']
    ordering = ['-post_date']



class AddPostView(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'adicionar_post.html'
    # fields = '__all__'
    # fields = ('title', 'body')


class DeletePostView(DeleteView):
    model = Post
    template_name = 'deletar_post.html'
    success_url = reverse_lazy('home')


class ConteudoPostView(DetailView):
    model = Post
    template_name = 'post_conteudo.html'


class EditarPostView(UpdateView):
    model = Post
    form_class = EditForm
    template_name = 'editar_post.html'
    # fields = ['title', 'title_tag', 'body']



class AdicionarCommentView(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'adicionar_comentario.html'
    ordering = ['-id']
    

    def form_valid(self, form):
        form.instance.post_id = self.kwargs['pk']
        return super().form_valid(form)
    
    success_url = reverse_lazy('home')


    # fields = '__all__'
    # fields = ('title', 'body')

# class CategoryListView(ListView):
#     model = Category
#     template_name = 

def Category_Posts(request, category_id):
    category = get_object_or_404(Category, pk = category_id)
    context = {'category': category}
    return render(request, 'categories.html', context)


class CategoryListView(ListView):
    model = Category
    template_name = "categories_separadas.html"

class AdicionarCategoryView(CreateView):
    model = Category
    # form_class = PostForm
    template_name = 'categoria_adicionar.html'
    fields = '__all__'
    # fields = ('title', 'body')

